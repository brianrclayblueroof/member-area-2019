// Ave Price Chart 

var avePriceData = [300000, 450000, 400000, 350000, 300000, 400000];

var leftValue = avePriceData[0];
var rightValue = avePriceData[5];

$(document).ready(function(){
    avePriceData.unshift(leftValue);
    avePriceData.push(rightValue);

    Chart.pluginService.register({
        beforeRender: function (chart) {
            if (chart.config.options.showAllTooltips) {
                // create an array of tooltips
                // we can't use the chart tooltip because there is only one tooltip per chart
                chart.pluginTooltips = [];
                chart.config.data.datasets.forEach(function (dataset, i) {
                    chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                        chart.pluginTooltips.push(new Chart.Tooltip({
                            _chart: chart.chart,
                            _chartInstance: chart,
                            _data: chart.data,
                            _options: chart.options.tooltips,
                            _active: [sector]
                        }, chart));
                    });
                });
    
                // turn off normal tooltips
                chart.options.tooltips.enabled = false;
            }
        },
        afterDraw: function (chart, easing) {
            if (chart.config.options.showAllTooltips) {
                // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                if (!chart.allTooltipsOnce) {
                    if (easing !== 1)
                        return;
                    chart.allTooltipsOnce = true;
                }
    
                // turn on tooltips
                chart.options.tooltips.enabled = true;
                Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                    // console.log(tooltip)
                    tooltip.initialize();
                    tooltip.update();
                    // we don't actually need this since we are not animating tooltips
                    tooltip.pivot();
                    tooltip.transition(easing).draw();
                });
                chart.options.tooltips.enabled = false;
            }
        }
    });
    
    var avePriceChartOptions = {
        maintainAspectRatio: false,
        showAllTooltips: true,
        title: {
            display: false
        },
        legend: {
            display: false
        },
        layout: {
            padding: {top: 60}
        },
        tooltips: {
            position: 'average',
            backgroundColor: 'transparent',
            bodyFontColor: '#818181',
            bodyFontSize: 15,
            displayColors: false,
            yAlign: 'top',
            xAlign: 'center',
            yPadding: 13,
            callbacks: {
                title: function(){
                    return false
                },
                label: function (tooltipItem, data) {
                    if(tooltipItem.xLabel == ""){
                        tooltipItem.yLabel = "";
                    } else {
                        return numeral(Number(data.datasets[0].data[tooltipItem.index])).format('$0,0');
                    }
                    
                }
            }
        },
        scales: {
            xAxes: [{
                display: true,
                position: 'top',
                gridLines: {
                    display: true,
                    drawBorder: false,
                    tickMarkLength: 20
                },
                ticks: {
                    fontSize: 15,
                    fontColor: '#818181',
                    callback: function (value, index, values) {
                        if (index !== 0 && index !== 7) {
                            return value;
                        } else {
                            return '';
                        }
                    }
                },
                afterBuildTicks: function(axis) {
                    axis.margins.left = '-5px';
                    axis.margins.right = '-5px';
                }
            }],
            yAxes: [{
                display: false,
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true,
                    suggestedMax: (Math.max.apply(null, avePriceData) + 1),
                }
            }]
        }
    };
    var ctx2 = document.getElementById("ave-price-chart").getContext('2d');
    var avePriceChart = new Chart(ctx2, {
        type: 'line',
        data: {
            labels: ["May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [{
                backgroundColor: '#ffffff',
                data: avePriceData,
                pointBackgroundColor: "#4D8AF0",
                pointBorderColor: "transparent",
                pointRadius: [0, 5, 5, 5, 5, 5, 5, 0],
                borderColor: '#ffffff'
            }
            ],
        },
        options: avePriceChartOptions,
    });
});

// Property Types Chart

var propertyTypesData = [100, 120, 30];

var propertyTypesChartOptions = {
    title: {
        display: false
    },
    legend: {
        display: false
    },
    cutoutPercentage: 80,
    maintainAspectRatio: false
};
var ctx2 = document.getElementById("property-types-chart").getContext('2d');
var propertyTypesChart = new Chart(ctx2, {
    type: 'doughnut',
    data: {
        labels: ['Single Family', 'Townhouse/Condo', 'Other'],
        datasets: [{
            backgroundColor: ['#3D6DBF', "#4D8AF0", "#8CB6FE"],
            data: propertyTypesData
        }
        ],
    },
    options: propertyTypesChartOptions
});

// Days on Market

var daysOnMarketData = [100, 120, 30, 100, 120, 30];

var daysOnMarketChartOptions = {
    title: {
        display: false
    },
    legend: {
        display: false
    },
    scales: {
        xAxes: [{
            gridLines: {
                display: false
            }
        }],
        yAxes: [{
            gridLines: {
                display: false
            },
            ticks: {
                beginAtZero: true
            }
        }]
    }
};
var ctx2 = document.getElementById("days-on-market-chart").getContext('2d');
var daysOnMarketChart = new Chart(ctx2, {
    type: 'bar',
    data: {
        labels: ['Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'NOv'],
        datasets: [{
            backgroundColor: '#4D8AF0',
            data: daysOnMarketData
        }
        ],
    },
    options: daysOnMarketChartOptions
});

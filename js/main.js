// Set Primary Residence

$('body').on('click', '.set-primary', function(){
    $('.set-primary').removeClass('active');
    $(this).addClass('active');
    console.log('hi');
});

// Show Tabs

$('body').on('click', '.tab-trigger', function (e){
    var tabToShow = $(e.target).attr('data-tab');
    var tabGroup;
    // hvChart.render();
    // equityChart.destroy();
    // equityChart.render();
    // console.log(equityChart.render())
    if($(e.target).attr('data-tab-group')){
        tabGroup = $(e.target).attr('data-tab-group');
        var targetTab = $(e.target);
        $('.tab-pane').each(function(){
            if($(this).attr('data-tab-group') == tabGroup){
                $(this).removeClass('open');
            }
            $('#' + tabToShow).addClass('open');
        });
        $('.tab-trigger').each(function(){
            if($(this).attr('data-tab-group') == tabGroup){
                $(this).removeClass('active');
            }
            $(targetTab).addClass('active');
        });
    } else{
        $('.tab-pane').removeClass('open');
        $('#' + tabToShow).addClass('open');
        $('.tab-trigger').removeClass('active');
        $(this).addClass('active');
    }
});

// Comps Slider

$('.comps-slider').slick({
    slidesToShow: 3,
    arrows: true,
    nextArrow: $('.arrow.next'),
    prevArrow: $('.arrow.prev'),
    infinite: false,
    responsive: [
        {
            breakpoint: 1500,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 1000,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});
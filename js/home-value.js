// Set Primary Residence

$('body').on('click', '.set-primary', function(){
    $('.set-primary').removeClass('active');
    $(this).addClass('active');
    console.log('hi');
});

//Future Plans Select

$('body').on('click', '.plans-wrapper .plan', function(){
    $('.plans-wrapper .plan').removeClass('active');
    $(this).addClass('active');
    var type = $(this).attr('id');

    //Sell
    if(type == 'sell' && $('#sell').hasClass('active')){
        $('#sell-config').addClass('open');
    } else{
        $('#sell-config').removeClass('open');
    }
    
    //Rent
    if(type == 'rent' && $('#rent').hasClass('active')){
        $('#rent-config').addClass('open');
    } else{
        $('#rent-config').removeClass('open');
    }
    
    //Remodel
    if(type == 'remodel' && $('#remodel').hasClass('active')){
        $('#remodel-config').addClass('open');
    } else{
        $('#remodel-config').removeClass('open');
    }
});

// Home Value Chart
var hvCityData = [400000, 510000, 280000, 390000, 400000, 420000];
var hvCompsData = [300000, 310000, 280000, 290000, 300000, 320000];
var homeEstimate = [300000, 300000, 300000, 300000, 300000, 300000];
var hvCombinedData = hvCityData.concat(hvCompsData, homeEstimate);
var priceMax = (Math.max.apply(null, hvCombinedData));
var priceMin = (Math.min.apply(null, hvCombinedData));
var priceFormat = function(){
    if( priceMax > 1000000) {
        return '($0.0a)';
    } else{
        return '($0a)';
    }
};


var hvChartOptions = {
    maintainAspectRatio: false,
    bezierCurve: false,
    title: {
        display: false
    },
    legend: {
        display: false
    },
    tooltips: {
        mode: 'index',
        intersect: false,
        callbacks: {
            label: function (tooltipItem, data) {
                return tooltipItem.datasetIndex, data.datasets[tooltipItem.datasetIndex].label + ": " + numeral(Number(tooltipItem.yLabel)).format('$0,0');
            }
        }
    },
    scales: {
        xAxes: [{
            gridLines: {
                display: false
            }
        }],
        yAxes: [{
            ticks: {
                suggestedMin: (Math.min.apply(null, hvCombinedData) - 50000),
                suggestedMax: (Math.max.apply(null, hvCombinedData) + 50000),
                userCallback: function (value, index, values) {
                    value = numeral(value).format(priceFormat());
                    return value;
                }
            }
        }],
    },
    plugins: {
        datalabels: {
            display: false
        }
    }
};
var ctx = document.getElementById("hv-chart").getContext('2d');
var hvChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["Jun", "Jul", "Sep", "Oct", "Nov", "Dec",],
        datasets: [{
            label: 'Your Current Value',
            backgroundColor: "transparent",
            borderColor: "#179EFF",
            borderDash: [10, 5],
            data: homeEstimate,
            pointBorderColor: 'transparent'
        },
        {
            label: 'Eagle Mountain',
            backgroundColor: "rgba(122, 198, 255, .4)",
            borderColor: "transparent",
            data: hvCityData,
            pointBackgroundColor: '#47b2ff'
        },
            {
            label: 'Comp. Homes',
            backgroundColor: "rgba(122, 198, 255, .6)",
            borderColor: "transparent",
            data: hvCompsData,
            pointBackgroundColor: '#47b2ff'
        }
        ],
    },
    options: hvChartOptions
});


$('body').one('click', '.equity-tab-trigger', function (e){

    // Equity Chart 

    var equityData = [14000, 19000];

    var equityChartOptions = {
    title: {
        display: false
    },
    legend: {
        display: false
    },
    cutoutPercentage: 72,
    tooltips: {
        position: 'average',
        callbacks: {
            label: function (tooltipItem, data) {
                // console.log(data.labels[tooltipItem.index])
                // console.log(data.datasets[0].data[tooltipItem.index])
                return data.labels[tooltipItem.index] + ": " + numeral(Number(data.datasets[0].data[tooltipItem.index])).format('$0,0');
            }
        }
    }
    };
    var ctx2 = document.getElementById("equity-chart").getContext('2d');
    var equityChart = new Chart(ctx2, {
    type: 'doughnut',
    data: {
        labels: ['Toward Interest', 'Toward Principal'],
        datasets: [{
            backgroundColor: ['#F58E44', "#4D8AF0"],
            data: equityData
        }
        ],
    },
    options: equityChartOptions
    });
});

//Remodel Select

$('.remodel-select .item').click(function(){
    $(this).toggleClass('active');
});